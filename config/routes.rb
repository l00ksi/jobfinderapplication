Rails.application.routes.draw do
  resources :companies
  resources :job_applications
  get 'home/index'
  resources :job_postings

  get 'my_job_postings', to: 'job_postings#my_job_postings'
  get 'category_jobs', to: 'job_postings#category_jobs'
  get 'job_description', to: 'job_postings#job_description'



  devise_for :employers
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'home#index'

end
