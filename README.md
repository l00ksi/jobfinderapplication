To start the application for the first time:

Install gems  
run `bundle install`

Install yarn dependencies  
run `yarn install --check-files`  

Run migrations and populate database with seeds  
run `rails db:migrate`  
run `rails db:seed`

Finally start the server  
run `rails s`


