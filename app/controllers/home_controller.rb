class HomeController < ApplicationController
  def index
    @job_postings = JobPosting.where('expires_in > ?', DateTime.now).paginate(page: params[:page], per_page: 20)
    @app_name = 'Job Detector'
    @categories = Category.all
  end
end
