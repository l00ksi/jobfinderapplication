class JobPostingsController < ApplicationController
  before_action :set_job_posting, only: [:show, :edit, :update, :destroy, :job_description]
  before_action :authenticate_employer!, except: [:index, :show, :job_description, :category_jobs]


  # GET /job_postings
  # GET /job_postings.json
  def index
    @job_postings = JobPosting.all.paginate(page: params[:page], per_page: 20)
  end

  # GET /job_postings/1
  # GET /job_postings/1.json
  def show
  end

  # GET /job_postings/new
  def new
    @job_posting = JobPosting.new
    @qualifications = Qualification.all
  end

  # GET /job_postings/1/edit
  def edit
    @qualifications = Qualification.all
  end

  # POST /job_postings
  # POST /job_postings.json
  def create
    @job_posting = JobPosting.new(job_posting_params)
    @job_posting.employer_id = current_employer.id

    respond_to do |format|
      if @job_posting.save
        format.html { redirect_to @job_posting, notice: 'Job posting was successfully created.' }
        format.json { render :show, status: :created, location: @job_posting }
      else
        format.html { render :new }
        format.json { render json: @job_posting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_postings/1
  # PATCH/PUT /job_postings/1.json
  def update
    respond_to do |format|
      if @job_posting.update(job_posting_params)
        format.html { redirect_to @job_posting, notice: 'Job posting was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_posting }
      else
        format.html { render :edit }
        format.json { render json: @job_posting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_postings/1
  # DELETE /job_postings/1.json
  def destroy
    @job_posting.destroy
    respond_to do |format|
      format.html { redirect_to job_postings_url, notice: 'Job posting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def my_job_postings
    @job_postings = current_employer.job_postings.paginate(page: params[:page], per_page: 10)
  end

  def job_description
    @job_posting
    @expired = @job_posting.expires_in < DateTime.now
  end

  def category_jobs
    category = Category.find(params[:category_id])
    puts "HELLO!"
    render json: category.job_postings
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_posting
      @job_posting = JobPosting.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def job_posting_params
      params.require(:job_posting).permit(:employer_id, :title, :description, :email, :category_id, :expires_in, :qualification_id)
    end
end
