class JobApplicationsController < ApplicationController
  before_action :set_job_application, only: [:show, :edit, :update, :destroy, :download_cv]
  before_action :authenticate_employer!, except: [:new, :create]

  # GET /job_applications
  # GET /job_applications.json
  def index
    @job_applications = JobApplication.where('job_posting_id in (?)', current_employer.job_postings.ids)
  end

  # GET /job_applications/1
  # GET /job_applications/1.json
  def show
  end

  # GET /job_applications/new
  def new
    @job_application = JobApplication.new
    @job_application.job_posting_id = params[:job_posting_id]
    @qualifications = Qualification.all
  end

  # GET /job_applications/1/edit
  def edit
    @qualifications = Qualification.all
  end

  # POST /job_applications
  # POST /job_applications.json
  def create
    @job_application = JobApplication.new(job_application_params)

    respond_to do |format|
      if @job_application.save
        Services::MailSender.send_application_confirmations(@job_application)
        format.html { redirect_to root_path, notice: 'Job application was successfully created.' }
        format.json { render :show, status: :created, location: @job_application }
      else
        format.html { redirect_to new_job_application_path(job_posting_id: job_application_params[:job_posting_id]), alert: @job_application.errors.full_messages }
        format.json { render json: @job_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_applications/1
  # PATCH/PUT /job_applications/1.json
  def update
    respond_to do |format|
      if @job_application.update(job_application_params)
        format.html { redirect_to @job_application, notice: 'Job application was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_application }
      else
        format.html { render :edit }
        format.json { render json: @job_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_applications/1
  # DELETE /job_applications/1.json
  def destroy
    @job_application.destroy
    respond_to do |format|
      format.html { redirect_to job_applications_url, notice: 'Job application was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_application
      @job_application = JobApplication.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def job_application_params
      params.require(:job_application).permit(:first_name, :last_name, :birthdate, :email, :phone, :address, :qualification_id, :cv_file, :job_posting_id)
    end
end
