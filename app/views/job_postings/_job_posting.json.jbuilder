json.extract! job_posting, :id, :employer_id, :title, :description, :email, :category_id, :expires_in, :created_at, :updated_at
json.url job_posting_url(job_posting, format: :json)
