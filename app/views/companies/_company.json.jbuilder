json.extract! company, :id, :name, :owner_id, :business_description, :country, :city, :created_at, :updated_at
json.url company_url(company, format: :json)
