class JobApplicationMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def confirm_application_email
    @job_application = params[:job_application]
    @job_posting = @job_application.job_posting
    mail(to: @job_application.email, subject: 'Thank you for your application!')
  end

  def notify_employer_email
    @job_application = params[:job_application]
    @job_posting = @job_application.job_posting
    @url  = 'http://example.com/login'
    if ['test','development'].include? Rails.env
      @url = "http://localhost:3000/job_applications/#{@job_application.id}"
    end
    mail(to: @job_posting.employer.email, subject: 'New application for your job posting!')
  end
end
