class JobApplication < ApplicationRecord
  belongs_to :job_posting
  has_one_attached :cv_file
  validates_numericality_of :phone
  validates_presence_of :phone, :first_name, :last_name, :email, :address, :qualification_id, :job_posting_id, allow_blank: false
end
