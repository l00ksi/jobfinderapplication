class JobPosting < ApplicationRecord
  belongs_to :employer
  belongs_to :category
  belongs_to :qualification
  has_many :job_applications, dependent: :delete_all
  validates_presence_of :title, :description, :email, :expires_in
end
