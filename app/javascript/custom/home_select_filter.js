$(document).on('turbolinks:load', function() {
    let get_jobs;
    get_jobs = function(category_id) {
        let url;
        url = '/category_jobs';
        return $.ajax({
            type: 'GET',
            url: url,
            data: 'category_id=' + category_id,
            success: function(response) {
                console.log(response);
                $(".job_post").empty();
                $(".apple_pagination").remove()
                for(let i = 0; i < response.length; i++){
                    console.log("HI!")
                    $(".job_post").append("<li><a href='/job_description?id=" + response[0].id + "' class='category_job'>" + response[i].title + "</a> " +
                        response[i].description.slice(0, 100) + "..." +
                        "</li>");
                }
            },
            error: function(response) {
                console.log("Failed response");
            }
        });
    };

    $('#categories').change(function() {
        let category_id;
        category_id = $('#categories :selected').val();
        get_jobs(category_id);
    });
});