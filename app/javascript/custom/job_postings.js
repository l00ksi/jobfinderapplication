$(document).on('turbolinks:load', function() {
    $('#datetimepicker13').datetimepicker({
        inline: true,
        sideBySide: true,
        locale: 'en',
        stepping: 30,
        format: 'dddd, MMMM Do YYYY, HH:mm'
    });

    $('#datetimepicker13').on('change.datetimepicker', function () {
        $("#job_posting_expires_in")[0].value = $(this).datetimepicker('date')
    });
});
