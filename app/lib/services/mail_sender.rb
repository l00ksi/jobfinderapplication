module Services
  class MailSender
    def self.send_application_confirmations(job_application)
      JobApplicationMailer.with(job_application: job_application).confirm_application_email.deliver_later
      JobApplicationMailer.with(job_application: job_application).notify_employer_email.deliver_later
    end
  end
end


