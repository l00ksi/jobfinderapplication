module Mocks
  class EmployerMock
    def self.mock_object options={}
      Employer.new(
        first_name: 'Demo',
        last_name: 'Employer',
        email: options[:email] || 'd.employer@mail.com',
        password: 'test_pass'
      )
    end

    def self.mock_with_invalid_mail
      self.mock_object email: 'd.employer.mail.com'
    end

    def self.create_mock_object options={}
      obj = self.mock_object options
      obj.save
      Employer.last
    end
  end
end

