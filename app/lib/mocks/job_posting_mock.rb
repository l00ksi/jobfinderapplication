module Mocks
  class JobPostingMock
    def self.mock_object options={}
      employer = options[:employer] || EmployerMock.mock_object
      category = options[:category] || CategoryMock.mock_object

      JobPosting.new(
        employer: employer.id,
        title: options[:title] || 'Test engineer',
        description: options[:description] || 'Sample description',
        email: options[:email] || 'myemail@mail.com',
        category_id: category.id,
        expires_in: options[:expires_in] || DateTime.now + 7.days,
        qualification_id: options[:qualification_id] || Qualification.first.id
      )
    end

    def self.create_mock_object options={}
      obj = self.mock_object options
      obj.save
      JobPosting.last
    end
  end
end

