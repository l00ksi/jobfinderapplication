module Mocks
  class CategoryMock
    @@category_names = ['Information Technology', 'Medicine', 'Physics', 'Education', 'Construction', 'Transport', 'Agriculture']

    def self.mock_object
      Category.create name: @@category_names.sample
    end
  end
end
