# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

categories = ['Information Technology', 'Medicine', 'Physics', 'Education', 'Construction', 'Transport', 'Agriculture']

categories.each do |category_name|
  Category.create(name: category_name)
end

job_qualifications = ['Masters degree', 'Bachelor degree', 'High school']
job_qualifications.each { |qualification| Qualification.create(title: qualification)  }

employers = [{name: 'Ivan', surname: 'Ivić', email: 'poslodavac@gmail.com', password: 'ivanivan'},
  {name: 'Lilia', surname: 'Kirby', email: 'liliakirby@hotmail.com', password: 'lililili'}]

employers.each do |employer|
  Employer.create(first_name: employer[:name], last_name: employer[:last_name], email: employer[:email], password: employer[:password])
end

(0..100).each do |i|
  JobPosting.create(employer_id: Employer.all.ids.sample, title: "Job #{i} in a row", description: "The best job ever!",
    email: Employer.all.pluck(:email).sample, category_id: Category.all.ids.sample, qualification_id: Qualification.all.ids.sample,
    expires_in: DateTime.now + 1.month
  )
end

(1..20).each do |i|
  JobApplication.create(first_name: "First", last_name: "Last", birthdate: DateTime.now, email: "first#{i}@mail.com", phone: "0999999999",
    address: "Trg Bana Jelačića #{i}", job_posting_id: JobPosting.last(i)[0].id, qualification_id: JobPosting.last(i)[0].qualification_id
  )
end