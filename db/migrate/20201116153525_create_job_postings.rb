class CreateJobPostings < ActiveRecord::Migration[6.0]
  def change
    create_table :job_postings do |t|
      t.integer :employer_id
      t.string :title
      t.string :description
      t.string :email
      t.integer :category_id
      t.datetime :expires_in

      t.timestamps
    end
  end
end
