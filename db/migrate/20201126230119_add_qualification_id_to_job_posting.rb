class AddQualificationIdToJobPosting < ActiveRecord::Migration[6.0]
  def change
    add_column :job_postings, :qualification_id, :integer
  end
end
