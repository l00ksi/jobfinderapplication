class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :owner_id
      t.string :business_description
      t.string :country
      t.string :city

      t.timestamps
    end
  end
end
