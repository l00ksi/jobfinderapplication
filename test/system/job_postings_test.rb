require "application_system_test_case"

class JobPostingsTest < ApplicationSystemTestCase
  setup do
    @employer = Mocks::EmployerMock.create_mock_object
    @job_posting = Mocks::JobPostingMock.mock_object employer_id: @employer.id
  end

  test "visiting the index" do
    visit job_postings_url
    assert_selector "h1", text: "Job Postings"
  end

  test "creating a Job posting" do
    visit job_postings_url
    click_on "New Job Posting"

    fill_in "Category", with: @job_posting.category_id
    fill_in "Description", with: @job_posting.description
    fill_in "Email", with: @job_posting.email
    fill_in "Employer", with: @job_posting.employer_id
    fill_in "Expires in", with: @job_posting.expires_in
    fill_in "Title", with: @job_posting.title
    click_on "Create Job posting"

    assert_text "Job posting was successfully created"
    click_on "Back"
  end

  test "updating a Job posting" do
    visit job_postings_url
    click_on "Edit", match: :first

    fill_in "Category", with: @job_posting.category_id
    fill_in "Description", with: @job_posting.description
    fill_in "Email", with: @job_posting.email
    fill_in "Employer", with: @job_posting.employer_id
    fill_in "Expires in", with: @job_posting.expires_in
    fill_in "Title", with: @job_posting.title
    click_on "Update Job posting"

    assert_text "Job posting was successfully updated"
    click_on "Back"
  end

  test "destroying a Job posting" do
    visit job_postings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job posting was successfully destroyed"
  end
end
